## Getting started

## Project Name

Opslag van bestanden in SharePoint

## Description

### NL

Het doel van het project is om een applicatie te ontwikkelen binnen Salesforce die ervoor gaat zorgen dat gebruikers gemakkelijk files op een gelinkte SharePoint kan raadplegen, aanpassen, verwijderen en toevoegen.
Er zal een frontend worden voorzien waar de gebruiker de voorziene functionaliteiten kan toepassen. In de backend zal er autorisatie worden voorzien om verschillende rollen en rechten toe te kennen aan de gebruikers.

### ENG

The goal of the project is to develop an application within Salesforce that will allow users to easily access, modify, delete and add files on a linked SharePoint.
A frontend will be provided where the user can apply the provided functionalities. In the backend there will be authorization to assign different roles and rights to the users.

## Installation

Install the managed package in your Salesforce org.

## Documentation

Frontend: [Documentation](https://gitlab.com/DePotterr/opslag-van-bestanden-in-sharepoint/-/tree/main/docs/frontend)

Backend: [Documentation](https://gitlab.com/DePotterr/opslag-van-bestanden-in-sharepoint/-/tree/main/docs/backend)

## Authors and acknowledgment

Robin de Potter

## License

Licensed under The Customer Link.
