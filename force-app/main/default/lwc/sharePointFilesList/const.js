import { getActions } from "./permissions";

/**
 * @constant
 * @memberof SharePointFilesList
 * @default "Check file for default value"
 */
const COLUMNS = [
  {
    label: "Name",
    fieldName: "webUrl",
    type: "url",
    typeAttributes: { label: { fieldName: "name" } },
    cellAttributes: {
      iconName: { fieldName: "iconName" },
      iconPosition: "left"
    }
    // initialWidth: 50
  },
  {
    label: "Last Modified",
    fieldName: "modified"
    // type: "date"
  },
  {
    label: "Created on",
    fieldName: "createdDateTime"
    // type: "date"
  },
  {
    label: "Size",
    fieldName: "size"
  },
  // {
  //   type: "button-icon",
  //   typeAttributes: {
  //     iconName: "utility:delete",
  //     name: "delete",
  //     iconClass: "slds-icon-text-error"
  //   }
  // },
  // {
  //   type: "button-icon",
  //   typeAttributes: {
  //     iconName: "utility:download",
  //     name: "download"
  //   }
  // },
  {
    type: "action",
    typeAttributes: { rowActions: getActions() }
  }
];

/**
 * @constant
 * @default
 * @memberof SharePointFilesList
 */
const ACCEPTED_FILES = "*";

/**
 * @constant
 * @default
 * @memberof SharePointFilesList
 */
const MAX_FILES = 10;

/**
 * @constant
 * @default
 * @memberof SharePointFilesList
 */
const MAX_FILE_SIZE = 3145728;

/**
 * @constant
 * @default
 * @memberof SharePointFilesList
 */
const RECORD_FIELDS = ["Account.Name"];

export { COLUMNS, ACCEPTED_FILES, MAX_FILES, MAX_FILE_SIZE, RECORD_FIELDS };
