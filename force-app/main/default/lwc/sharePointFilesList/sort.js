/**
 * @method SharePointFilesList.sortOnName
 * @param {Object} a - First row.
 * @param {Object} b - Second row.
 * @description It sorts the datatable descending on name.
 * -1: lower,
 * 1: higher,
 * 0: same
 * @return {int} Sort value.
 */
const sortOnName = (a, b) => {
  const nameA = a.name.toUpperCase();
  const nameB = b.name.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
};
export { sortOnName };
