const CURRENT_PAGE_REFERENCE = {
  type: "standard__recordPage",
  attributes: {
    objectApiName: "Account",
    recordId: "0017R00002mMMZ2QAO",
    actionName: "view"
  },
  state: { c__path: "" }
};
export default CURRENT_PAGE_REFERENCE;
