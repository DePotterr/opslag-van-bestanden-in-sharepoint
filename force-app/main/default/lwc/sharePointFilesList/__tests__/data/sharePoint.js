const CURRENT_SHAREPOINT = {
  Id: "a017R0000461kr3QAA",
  Name: "TheCustomerLink",
  Active__c: true,
  Base_Url__c: "12. TCL Academy/Stages/Files",
  SharePoint_Site_Id__c: "367dd56b-8d25-403f-87c2-cd447bb60ad3",
  Link__c:
    "https://cronos.sharepoint.com/sites/TheCustomerLink/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FTheCustomerLink%2FShared%20Documents%2F12%2E%20TCL%20Academy%2FStages%2FFiles"
};

export default CURRENT_SHAREPOINT;
