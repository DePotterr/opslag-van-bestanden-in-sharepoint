const ACCOUNT = {
  apiName: "Account",
  childRelationships: {},
  fields: {
    Name: {
      displayValue: null,
      value: "Robin de Potter"
    }
  },
  id: "0017R00002mMMZ2QAO",
  lastModifiedById: "0057R00000AvFIaQAN",
  lastModifiedDate: "2022-04-12T08:35:23.000Z",
  recordTypeId: "012000000000000AAA",
  recordTypeInfo: null,
  systemModstamp: "2022-04-12T08:35:23.000Z"
};

export default ACCOUNT;
