const MOCK_FILES = {
  "@odata.context":
    "https://graph.microsoft.com/v1.0/$metadata#sites('367dd56b-8d25-403f-87c2-cd447bb60ad3')/drive/root/children",
  value: [
    {
      createdDateTime: "2022-04-29T09:32:49Z",
      eTag: '"{2C917212-0458-40FE-B2FF-80DC6DC8D0A4},1"',
      id: "014EUULLQSOKISYWAE7ZALF74A3RW4RUFE",
      lastModifiedDateTime: "2022-04-29T09:32:49Z",
      name: "test",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/Shared%20Documents/12.%20TCL%20Academy/Stages/Files/Robin%20de%20Potter%20(0017R00002mMMZ2QAO)/test",
      cTag: '"c:{2C917212-0458-40FE-B2FF-80DC6DC8D0A4},0"',
      size: 0,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T09:32:49Z",
        lastModifiedDateTime: "2022-04-29T09:32:49Z"
      },
      folder: {
        childCount: 0
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=9823f388-4376-4b17-be33-71e6ae4d4988&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJXU1VMbDQ5M3l1MTVkVkhBaDJVRUM1a0h4OUd0UkFCZGNTMnkvb1IxQi9ZPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.MzhJOVZ5bFV4b1hPQkROVG45VGdsUkRobjB2MjBYdU1WWlZmd3d6cFJwVT0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:37Z",
      eTag: '"{9823F388-4376-4B17-BE33-71E6AE4D4988},1"',
      id: "014EUULLUI6MRZQ5SDC5F34M3R42XE2SMI",
      lastModifiedDateTime: "2022-04-29T08:37:37Z",
      name: "Blueprint.docx",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/Doc.aspx?sourcedoc=%7B9823F388-4376-4B17-BE33-71E6AE4D4988%7D&file=Blueprint.docx&action=default&mobileredirect=true",
      cTag: '"c:{9823F388-4376-4B17-BE33-71E6AE4D4988},1"',
      size: 2913357,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        hashes: {
          quickXorHash: "GOfxaNtEAhxZeI7BD+5tzuHjPtE="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:37Z",
        lastModifiedDateTime: "2022-04-29T08:37:37Z"
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=3704dd8a-7d4a-4837-b289-d0f44e23dcf0&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJKQUxEcktmZVBHS1llbjhDMWlYRkE0MFNRYmVwTWhpMytFS0xZQmM4bnp3PSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.TmN4NmlhbUZMNlp2YmxvbHNtRjFJN1RkN0RnamVTVFhHY000MVdNSHg4VT0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:38Z",
      eTag: '"{3704DD8A-7D4A-4837-B289-D0F44E23DCF0},1"',
      id: "014EUULLUK3UCDOST5G5ELFCOQ6RHCHXHQ",
      lastModifiedDateTime: "2022-04-29T08:37:38Z",
      name: "Blueprint.pdf",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/Shared%20Documents/12.%20TCL%20Academy/Stages/Files/Robin%20de%20Potter%20(0017R00002mMMZ2QAO)/Blueprint.pdf",
      cTag: '"c:{3704DD8A-7D4A-4837-B289-D0F44E23DCF0},1"',
      size: 2507148,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType: "application/pdf",
        hashes: {
          quickXorHash: "y5cNfZpX0TzWs5Nh9DgjRuo7E7Y="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:38Z",
        lastModifiedDateTime: "2022-04-29T08:37:38Z"
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=f647bbb9-753f-43a6-a097-e6fdaecff4ca&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJaWS9sektuazhvcmtTZ2w3Y25KS2w0dkk0SWV3Tlh0dG91VFhvaVk2YktjPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.ckhyL1g2bWdPY3Rac0hocWNZWHpQcDVmWXNESUdXaUZRK2ZJd0h4TnhWND0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:36Z",
      eTag: '"{F647BBB9-753F-43A6-A097-E6FDAECFF4CA},4"',
      id: "014EUULLVZXND7MP3VUZB2BF7G7WXM75GK",
      lastModifiedDateTime: "2022-04-29T08:37:39Z",
      name: "Blueprint.pptx",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/Doc.aspx?sourcedoc=%7BF647BBB9-753F-43A6-A097-E6FDAECFF4CA%7D&file=Blueprint.pptx&action=edit&mobileredirect=true",
      cTag: '"c:{F647BBB9-753F-43A6-A097-E6FDAECFF4CA},3"',
      size: 7770420,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType:
          "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        hashes: {
          quickXorHash: "1gvRaDxDxF9ksx2stvm9mpdFhBg="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:36Z",
        lastModifiedDateTime: "2022-04-29T08:37:39Z"
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=5689ff15-32ca-4646-ae9b-9b2f6f06ba8c&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJ6QTJqY1F1MlBHVVAxUmZnN3ZPWUFOTDFnVm84UmowMTFXMUtnVUlaa3dBPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.L2oxUjZNSDdUK09sbCtEVktUUm1xb2tSMVBBbDEvL01vS2xCMVVranhDTT0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:35Z",
      eTag: '"{5689FF15-32CA-4646-AE9B-9B2F6F06BA8C},1"',
      id: "014EUULLQV76EVNSRSIZDK5G43F5XQNOUM",
      lastModifiedDateTime: "2022-04-29T08:37:35Z",
      name: "SessionSecurity.png",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/Shared%20Documents/12.%20TCL%20Academy/Stages/Files/Robin%20de%20Potter%20(0017R00002mMMZ2QAO)/SessionSecurity.png",
      cTag: '"c:{5689FF15-32CA-4646-AE9B-9B2F6F06BA8C},2"',
      size: 4480,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType: "image/png",
        hashes: {
          quickXorHash: "4dLeHDFOOSgfDVM6YNRkCc3JmAM="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:35Z",
        lastModifiedDateTime: "2022-04-29T08:37:35Z"
      },
      image: {}
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=fb52622f-b498-45f1-b1b4-d39cc50e8531&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJlL1B1enRVZ1pZUFpEaitFNTcydlR0R214RytleXhhcWk3Wnh3TFNNcDlRPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.Y01VblNXRHhPaUpRNkJZVmtVMXAxR1o1MERTOXlEZ2NtUlFONmo2cklodz0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:35Z",
      eTag: '"{FB52622F-B498-45F1-B1B4-D39CC50E8531},1"',
      id: "014EUULLRPMJJPXGFU6FC3DNGTTTCQ5BJR",
      lastModifiedDateTime: "2022-04-29T08:37:35Z",
      name: "Testplan.docx",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/Doc.aspx?sourcedoc=%7BFB52622F-B498-45F1-B1B4-D39CC50E8531%7D&file=Testplan.docx&action=default&mobileredirect=true",
      cTag: '"c:{FB52622F-B498-45F1-B1B4-D39CC50E8531},1"',
      size: 142314,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        hashes: {
          quickXorHash: "3e9Rq0BYdLJUWj3MoYp1Rmyulk0="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:35Z",
        lastModifiedDateTime: "2022-04-29T08:37:35Z"
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=1b91d071-9660-4832-8abd-055bef84e9f8&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJ6VVJ6dHMxRE9hdi8vZWZSUm5KZHZiaUpmVW5ZTUNmeTRJcHRjNURGbExVPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.Q1VsV3pOYzdoY1U2SGVVYVBlQmRZT2wzUDdQTHVpS2ZjNmtUTGdGQzRKYz0&ApiVersion=2.0",
      createdDateTime: "2022-04-29T08:37:35Z",
      eTag: '"{1B91D071-9660-4832-8ABD-055BEF84E9F8},1"',
      id: "014EUULLTR2CIRWYEWGJEIVPIFLPXYJ2PY",
      lastModifiedDateTime: "2022-04-29T08:37:35Z",
      name: "Testplan.pdf",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/Shared%20Documents/12.%20TCL%20Academy/Stages/Files/Robin%20de%20Potter%20(0017R00002mMMZ2QAO)/Testplan.pdf",
      cTag: '"c:{1B91D071-9660-4832-8ABD-055BEF84E9F8},1"',
      size: 343844,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType: "application/pdf",
        hashes: {
          quickXorHash: "n2CMkZERJryn3cDzPAx1STcFCy0="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-29T08:37:35Z",
        lastModifiedDateTime: "2022-04-29T08:37:35Z"
      }
    },
    {
      "@microsoft.graph.downloadUrl":
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/15/download.aspx?UniqueId=0fd6d612-c837-4843-9c9a-5482a0ff3bef&Translate=false&tempauth=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY3Jvbm9zLnNoYXJlcG9pbnQuY29tQDQ5YzNkNzAzLTM1NzktNDdiZi1hODg4LTdjOTEzZmJkY2VkOSIsImlzcyI6IjAwMDAwMDAzLTAwMDAtMGZmMS1jZTAwLTAwMDAwMDAwMDAwMCIsIm5iZiI6IjE2NTEyMjQ5OTYiLCJleHAiOiIxNjUxMjI4NTk2IiwiZW5kcG9pbnR1cmwiOiJtUjArQlB3YVRCK1dNeVpBak5oU2RPaWh6bk95K3V3QUQ3dlB6cFJqZElRPSIsImVuZHBvaW50dXJsTGVuZ3RoIjoiMTM5IiwiaXNsb29wYmFjayI6IlRydWUiLCJjaWQiOiJZVGszTkRsbFlURXRNRE14TkMwME1qZGhMVGt5Tm1FdE1EZGpZVE01TURJNFlqVXoiLCJ2ZXIiOiJoYXNoZWRwcm9vZnRva2VuIiwic2l0ZWlkIjoiTXpZM1pHUTFObUl0T0dReU5TMDBNRE5tTFRnM1l6SXRZMlEwTkRkaVlqWXdZV1F6IiwiYXBwX2Rpc3BsYXluYW1lIjoiU2FsZXNmb3JjZSBpbnRlZ3JhdGlvbiIsImdpdmVuX25hbWUiOiJSb2JpbiIsImZhbWlseV9uYW1lIjoiZGUgUG90dGVyIiwic2lnbmluX3N0YXRlIjoiW1wia21zaVwiXSIsImFwcGlkIjoiODNkMDJhOTAtMDc5Mi00YzExLWEwMTItNWQ3MzBhOTZmNmUxIiwidGlkIjoiNDljM2Q3MDMtMzU3OS00N2JmLWE4ODgtN2M5MTNmYmRjZWQ5IiwidXBuIjoiZGVwb3Ryb0Bjcm9ub3MuYmUiLCJwdWlkIjoiMTAwMzIwMDFEMzc5OTNBNyIsImNhY2hla2V5IjoiMGguZnxtZW1iZXJzaGlwfDEwMDMyMDAxZDM3OTkzYTdAbGl2ZS5jb20iLCJzY3AiOiJhbGxmaWxlcy5yZWFkIGFsbGZpbGVzLndyaXRlIGFsbHByb2ZpbGVzLnJlYWQiLCJ0dCI6IjIiLCJ1c2VQZXJzaXN0ZW50Q29va2llIjpudWxsLCJpcGFkZHIiOiI0MC4xMjYuMzIuOTkifQ.MnYvVjZUZXg3ZUsvTUdiTU9kQ3ZuQWMwNStKZGQzS2EyWUJ2aVBwYkl1Zz0&ApiVersion=2.0",
      createdDateTime: "2022-04-28T16:41:09Z",
      eTag: '"{0FD6D612-C837-4843-9C9A-5482A0FF3BEF},1"',
      id: "014EUULLQS23LA6N6IINEJZGSUQKQP6O7P",
      lastModifiedDateTime: "2022-04-28T16:41:09Z",
      name: "Testresultaten.docx",
      webUrl:
        "https://cronos.sharepoint.com/sites/TheCustomerLink/_layouts/Doc.aspx?sourcedoc=%7B0FD6D612-C837-4843-9C9A-5482A0FF3BEF%7D&file=Testresultaten.docx&action=default&mobileredirect=true",
      cTag: '"c:{0FD6D612-C837-4843-9C9A-5482A0FF3BEF},2"',
      size: 134356,
      createdBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      lastModifiedBy: {
        application: {
          id: "83d02a90-0792-4c11-a012-5d730a96f6e1",
          displayName: "Salesforce integration"
        },
        user: {
          email: "Robin.dePotter@thecustomerlink.be",
          id: "4c5ebeb8-414b-4672-8bed-b80edade3e83",
          displayName: "Robin de Potter"
        }
      },
      parentReference: {
        driveType: "documentLibrary",
        driveId:
          "b!a9V9NiWNP0CHws1Ee7YK05BVzMLbXjVBjlYR2sp-7Q50xr3IXI4XSKloi24-0E4N",
        id: "014EUULLRUYOKI2V64ERCLSGDBTV462RZT",
        path: "/drive/root:/12. TCL Academy/Stages/Files/Robin de Potter (0017R00002mMMZ2QAO)"
      },
      file: {
        mimeType:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        hashes: {
          quickXorHash: "vYBGLZP/9pnC4vPBzr9tMgKdw9g="
        }
      },
      fileSystemInfo: {
        createdDateTime: "2022-04-28T16:41:09Z",
        lastModifiedDateTime: "2022-04-28T16:41:09Z"
      }
    }
  ]
};

export default MOCK_FILES;
