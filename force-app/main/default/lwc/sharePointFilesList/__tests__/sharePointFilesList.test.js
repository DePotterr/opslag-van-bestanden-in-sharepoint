import { createElement } from "lwc";
import SharePointFilesList from "c/sharePointFilesList";
import getCurrentSharePoint from "@salesforce/apex/SharePointSettingsService.getCurrentSharePoint";
import getDriveItems from "@salesforce/apex/SharePointService.getDriveItems";
import { loadScript } from "lightning/platformResourceLoader";

import MOCK_FILES from "./data/driveItems";
import CURRENT_SHAREPOINT from "./data/sharePoint";
import moment from "../../../staticresources/moment";

jest.mock(
  "@salesforce/apex/SharePointSettingsService.getCurrentSharePoint",
  () => {
    return {
      default: jest.fn()
    };
  },
  { virtual: true }
);

jest.mock(
  "@salesforce/apex/SharePointService.getDriveItems",
  () => {
    return {
      default: jest.fn()
    };
  },
  { virtual: true }
);

jest.mock(
  "lightning/platformResourceLoader",
  () => {
    return {
      loadScript() {
        return new Promise((resolve) => {
          global.moment = require("../../../staticresources/moment");
          resolve();
        });
      },
      loadStyle() {
        return new Promise((resolve) => {
          global.dropArea = require("../../../staticresources/dropArea.css");
          global.customDataTable = require("../../../staticresources/customDataTable.css");
          resolve();
        });
      }
    };
  },
  { virtual: true }
);

describe("c-share-point-files-list", () => {
  async function flushPromises() {
    return Promise.resolve();
  }

  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
    jest.clearAllMocks();
  });

  it("Retrieves current SharePoint", async () => {
    getCurrentSharePoint.mockResolvedValue(CURRENT_SHAREPOINT);
    const currentSharePoint = await getCurrentSharePoint();
    expect(currentSharePoint).toBe(CURRENT_SHAREPOINT);
  });

  it("Retrieves files", async () => {
    getDriveItems.mockResolvedValue(MOCK_FILES);
    const driveItems = await getDriveItems();
    expect(driveItems).toBe(MOCK_FILES);
  });

  it("loads script", async () => {
    await loadScript();
    expect(global.moment).toBe(moment);
  });

  it("updates driveItems", async () => {
    const element = createElement("c-share-point-files-list", {
      is: SharePointFilesList
    });
    document.body.appendChild(element);
    element._driveItems = MOCK_FILES;
    await flushPromises();
    expect(element._driveItems).toBe(MOCK_FILES);
  });
});
