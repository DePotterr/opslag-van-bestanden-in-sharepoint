import hasDeletePermission from "@salesforce/customPermission/SharePoint_Delete_File";

/**
 * @method SharePointFilesList.getActions
 * @description It creates the actions for the datatable based on the permission an user has.
 * @return {Array} Array of actions.
 */
export const getActions = () => {
  let actions = [
    {
      label: "Download",
      name: "download",
      iconName: "utility:download"
    }
  ];
  if (hasDeletePermission) {
    actions.push({
      label: "Delete",
      name: "delete",
      iconName: "utility:delete"
    });
  }
  return actions;
};
