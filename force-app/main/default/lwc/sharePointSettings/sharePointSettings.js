import { LightningElement, track } from "lwc";
import getSharePointSettings from "@salesforce/apex/SharePointSettingsService.getSharePointSettings";
import changeSharePoint from "@salesforce/apex/SharePointSettingsService.changeSharePoint";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import createSharePoint from "@salesforce/apex/SharePointSettingsService.createSharePoint";
import { loadStyle, loadScript } from "lightning/platformResourceLoader";
import sharePointSettings from "@salesforce/resourceUrl/sharePointSettings";
import deleteSharePoint from "@salesforce/apex/SharePointSettingsService.deleteSharePoint";
import updateSharePoints from "@salesforce/apex/SharePointSettingsService.updateSharePoints";
import moment from "@salesforce/resourceUrl/moment";

/**
 * @constant
 * @default
 * @memberof Settings
 */
const ACTIONS = [
  { label: "Delete", name: "delete" },
  { label: "Show Information", name: "information" }
];

/**
 * @constant
 * @default "Check file for default value"
 * @memberof Settings
 */
const COLUMNS = [
  {
    label: "Name",
    fieldName: "Name",
    cellAttributes: {
      iconName: { fieldName: "iconName" },
      iconPosition: "right"
    },
    editable: true
  },
  {
    label: "Base Url",
    fieldName: "Base_Url__c",
    editable: true
  },
  { label: "SharePoint Link", fieldName: "Link__c", editable: true },
  { label: "Last Modified On", fieldName: "LastModifiedDate" },
  { label: "Created On", fieldName: "CreatedDate" },
  {
    label: "Activate",
    type: "button",
    typeAttributes: {
      label: "Activate",
      name: "activate",
      disabled: { fieldName: "Active__c" },
      variant: { fieldName: "variant" }
    }
  },
  {
    type: "action",
    typeAttributes: { rowActions: ACTIONS }
  }
];

/**
 * This component lets an user interact with SharePoint.
 * @alias Settings
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-settings></c-settings>
 */
export default class Settings extends LightningElement {
  /**
   * SharePoint settings.
   * @type {Array}
   */
  @track settings;

  /**
   * Rows of the datatable.
   * @type {Array}
   */
  rows;

  /**
   * Columns of the datatable.
   * @type {Array}
   * @default
   */
  columns = COLUMNS;

  /**
   * Actions of the datatable.
   * @type {Array}
   * @default
   */
  actions = ACTIONS;

  /**
   * Current SharePoint.
   * @type {Object}
   */
  activeSharePoint;

  /**
   * Draft values. Values when data is edited in the datatable.
   * @type {Array}
   * @default
   */
  draftValues = [];

  /**
   * Determents when the initial values are loaded.
   * @type {boolean}
   * @default
   */
  initialValuesLoaded = false;

  /**
   * @event Settings.connectedCallback
   * @description Loads initial data such as styles, scripts, data from Salesforce, etc.
   */
  connectedCallback() {
    this.getSettingsData();
    Promise.all([
      loadStyle(this, sharePointSettings),
      loadScript(this, moment)
    ]).then(() => {
      this.initialValuesLoaded = true;
    });
  }

  /**
   * @method Settings.getSettingsData
   * @description It gets the settings from Salesforce. Those settings are saved in custom settings.
   */
  getSettingsData() {
    getSharePointSettings().then((data) => {
      this.settings = data;
      console.log(data);
      this.formatSettingsToRows();
    });
  }

  /**
   * @method Settings.formatSettingsToRows
   * @description This function will format the data to rows, so the datatable can read the data.
   */
  formatSettingsToRows() {
    let newRows = [];
    this.settings.forEach((setting) => {
      newRows.push({
        ...setting,
        CreatedDate: window
          .moment(setting.CreatedDate)
          .local()
          .format("DD/MM/YYYY h:mm:ss A"),
        LastModifiedDate: window
          .moment(setting.LastModifiedDate)
          .local()
          .calendar(),
        id: setting.Id, // id is already being used.
        iconName: setting.Active__c ? "utility:success" : "",
        variant: setting.Active__c ? "success" : ""
      });
      if (setting.Active__c === true) {
        this.activeSharePoint = setting;
      }
    });
    this.rows = newRows;
    console.log("Rows:", JSON.stringify(this.rows));
  }

  /**
   * @event Settings.handleRowAction
   * @param {Object} event - Event that holds the actions and row in the detail property.
   * @description this is the action event trigger.
   * It triggers when an user clicks on one of the actions in a row.
   * The event holds which action is clicked.
   * Based on the action, a function will execute.
   * The available actions are: activate, delete and information.
   */
  handleRowAction(event) {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    switch (actionName) {
      case "activate":
        changeSharePoint({
          id: row.id
        }).then(() => {
          this.getSettingsData();
          const toastEvent = new ShowToastEvent({
            success: "Success",
            message: row.Name + " is now the current SharePoint.",
            variant: "success"
          });
          this.dispatchEvent(toastEvent);
        });
        break;
      case "delete":
        this.template.querySelector("c-delete-share-point-modal").show(row);
        break;
      case "information":
        this.template
          .querySelector("c-information-share-point-modal")
          .show(row);
        break;
      default:
    }
  }

  /**
   * @event Settings.handleOpenAddSharePointModal
   * @description This event will open the add SharePoint modal.
   */
  handleOpenAddSharePointModal() {
    this.template.querySelector("c-add-share-point-modal").show();
  }

  /**
   * @event Settings.handleAddSharePoint
   * @param {Object} event - Event that holds the values returned from the add SharePoint modal.
   * @description This event will add a new SharePoint
   */
  async handleAddSharePoint(event) {
    try {
      let data = await createSharePoint({
        name: event.detail.name,
        hostname: event.detail.hostname,
        baseUrl: event.detail.baseUrl,
        sitename: event.detail.sitename
      });
      console.log(data);
      this.getSettingsData();
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Success",
          message: "SharePoint created",
          variant: "success"
        })
      );
    } catch (error) {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error",
          message: error.body.message,
          variant: "error"
        })
      );
    }
  }

  /**
   * @event Settings.handleSave
   * @param {Object} event - Event that holds the draft values. It is triggered when the new values are submitted in the datatable.
   * @description This event will update the SharePoint.
   */
  async handleSave(event) {
    const updatedFields = event.detail.draftValues;
    console.log(updatedFields);

    try {
      const result = await updateSharePoints({ data: updatedFields });
      console.log(JSON.stringify("Apex update result: " + result));
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Success",
          message: "SharePoint updated",
          variant: "success"
        })
      );
      this.draftValues = [];
      this.getSettingsData();
    } catch (error) {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error updating or refreshing records",
          message: error.body.message,
          variant: "error"
        })
      );
    }
  }

  /**
   * @event Settings.handleDeleteSharePoint
   * @param {Object} event - Event that holds a SharePoint.
   * @description This event will delete a SharePoint. It triggers when the delete modal gets submitted.
   */
  handleDeleteSharePoint(event) {
    this.deleteSharePoint(event.detail.sharePoint);
  }

  /**
   * @method Settings.deleteSharePoint
   * @description This function will delete a SharePoint.
   */
  async deleteSharePoint(sharePoint) {
    try {
      await deleteSharePoint({ id: sharePoint.Id });
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Success",
          message: sharePoint.Name + " is deleted.",
          variant: "success"
        })
      );
      this.getSettingsData();
    } catch (error) {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "Error",
          message: error.body.message,
          variant: "error"
        })
      );
    }
  }
}
