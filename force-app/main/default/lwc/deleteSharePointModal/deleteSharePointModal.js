import { api, LightningElement } from "lwc";

/**
 * This modal will dispatch an event after confirmation. You can hook a function to that event.
 * It is a child component of Modal.
 * @alias DeleteSharePointModal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-delete-share-point-modal ondelete={function}></c-delete-share-point-modal>
 * Show the modal:
 * this.template.querySelector("c-delete-share-point-modal").show(SharePoint);
 */
export default class DeleteSharePointModal extends LightningElement {
  /**
   * SharePoint
   * @type {Object}
   */
  sharePoint;

  /**
   * @event DeleteSharePointModal.handleCancelClick
   * @description This event hides the modal. It triggers when the close button is clicked.
   */
  handleCancelClick() {
    this.template.querySelector("c-modal").hide();
  }

  /**
   * @event DeleteSharePointModal.handleDeleteClick
   * @description This event triggers a new event "delete" and hides the modal.
   */
  handleDeleteClick() {
    this.dispatchEvent(
      new CustomEvent("delete", { detail: { sharePoint: this.sharePoint } })
    );
    this.template.querySelector("c-modal").hide();
  }

  /**
   * @method DeleteSharePointModal.show
   * @param {Object} sharePoint - SharePoint
   * @description This method shows the modal with the provided SharePoint.
   */
  @api
  show(sharePoint) {
    this.sharePoint = sharePoint;
    this.template.querySelector("c-modal").show();
  }
}
