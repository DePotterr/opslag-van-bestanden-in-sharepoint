import { api, LightningElement } from "lwc";

/**
 * This component shows the SharePoint information.
 * @alias SharePointInformation
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-share-point-information current-share-point={sharePoint}></c-share-point-information>
 */
export default class SharePointInformation extends LightningElement {
  /**
   * Current(active) SharePoint.
   * @type {Object}
   */
  @api currentSharePoint;

  /**
   * Get property that returns the name of the current SharePoint..
   * @type {string}
   * @memberof SharePointInformation
   */
  get getSharePointName() {
    console.log(this.currentSharePoint);
    return this.currentSharePoint.Name;
  }
}
