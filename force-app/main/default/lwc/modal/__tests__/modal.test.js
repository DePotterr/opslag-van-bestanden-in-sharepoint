import { createElement } from "lwc";
import Modal from "c/modal";

describe("c-modal", () => {
  async function flushPromises() {
    return Promise.resolve();
  }

  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
  });

  it("sets a header to the modal", () => {
    // Arrange
    const element = createElement("c-modal", {
      is: Modal
    });
    document.body.appendChild(element);

    // Act
    element.header = "hello";

    // Assert
    expect(element.header).toBe("hello");
  });

  it("displays modal", async () => {
    // Arrange
    const element = createElement("c-modal", {
      is: Modal
    });
    document.body.appendChild(element);

    // Act
    element.show();

    // Assert
    await flushPromises();
    const div = element.shadowRoot.querySelector("div");
    console.log(div);
    expect(div).not.toBeNull();
  });

  it("changes header", async () => {
    const element = createElement("c-modal", {
      is: Modal
    });
    document.body.appendChild(element);

    element.show();
    element.header = "change";
    await flushPromises();

    const header = element.shadowRoot.querySelector("h2");
    expect(header.textContent).toBe("change");
  });
});
