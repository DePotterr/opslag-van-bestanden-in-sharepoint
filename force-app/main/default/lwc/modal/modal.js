import { LightningElement, api } from "lwc";

/**
 * @constant
 * @memberof Modal
 * @default
 */
const CSS_CLASS = "modal-hidden";

/**
 * Custom modal component.
 * @alias Modal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-modal header={"Hello World!"} modalSize{"Small"}></c-modal>
 */
export default class Modal extends LightningElement {
  /**
   * This property determents when the modal is visible. true = visible. false = hidden.
   * @type {boolean}
   * @default
   */
  isModalVisible = false;

  /**
   * This property determents if a header is assigned.
   * @type {boolean}
   * @default
   */
  hasHeaderString = false;

  /**
   * This property holds the header value
   * @type {string}
   */
  _headerPrivate;

  /**
   * Modal size. Small, Medium or Large.
   * @type {string}
   */
  @api modalSize;

  /**
   * Set property for header
   * @param value - contains the header
   * @memberof Modal
   */
  @api
  set header(value) {
    this.hasHeaderString = value !== "";
    this._headerPrivate = value;
  }

  /**
   * Get property that returns the header.
   * @memberof Modal
   * @return {string} Header.
   */
  get header() {
    return this._headerPrivate;
  }

  /**
   * @method Modal.show
   * @description This function will show the modal.
   */
  @api show() {
    this.isModalVisible = true;
  }

  /**
   * @method Modal.hide
   * @description This function will hide the modal.
   */
  @api hide() {
    this.isModalVisible = false;
  }

  /**
   * Get property that returns the class name based on the modal size property.
   * @memberof Modal
   * @return {string} Modal size class.
   */
  get modalClass() {
    let modalClass = "slds-modal slds-fade-in-open";

    if (this.modalSize) {
      if (this.modalSize === "Small") {
        modalClass += " slds-modal_small";
      } else if (this.modalSize === "Medium") {
        modalClass += " slds-modal_medium";
      } else if (this.modalSize === "Large") {
        modalClass += " slds-modal_large";
      }
    }
    return modalClass;
  }

  /**
   * @event Modal.handleDialogClose
   * @description This event will hide the modal and dispatch a new event named "closedialog".
   */
  handleDialogClose() {
    const closedialog = new CustomEvent("closedialog");
    this.dispatchEvent(closedialog);
    this.hide();
  }

  /**
   * @event Modal.handleSlotTaglineChange
   * @description This event will show the tagline content when the slot changed.
   * To add html to the tagline, use the slot="tagline" tag.
   */
  handleSlotTaglineChange() {
    if (!this.isModalVisible) {
      return;
    }
    const taglineEl = this.template.querySelector("p.tagline");
    taglineEl.classList.remove(CSS_CLASS);
  }

  /**
   * @event Modal.handleSlotFooterChange
   * @description This event will show the footer content when the slot changed.
   * To add html to the footer, use the slot="footer" tag.
   */
  handleSlotFooterChange() {
    if (!this.isModalVisible) {
      return;
    }
    const footerEl = this.template.querySelector("footer");
    footerEl.classList.remove(CSS_CLASS);
  }
}
