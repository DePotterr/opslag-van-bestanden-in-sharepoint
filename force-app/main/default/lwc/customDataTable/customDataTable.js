import { api, LightningElement } from "lwc";

/**
 * In the future you may want to create your own datatable, because the lightning-datatable is not suitable for mobile devices.
 * A custom datatable gives your more flexibility to the customers needs.
 * @alias CustomDataTable
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-custom-data-table></c-custom-data-table>
 */
export default class CustomDataTable extends LightningElement {
  @api rows;
  @api columns;
}
