import { api, LightningElement } from "lwc";

/**
 * This component shows the SharePoint information.
 * It is a child component of Modal.
 * @alias InformationSharePointModal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-information-share-point-modal></c-information-share-point-modal>
 * In JavaScript:
 * this.template.querySelector("c-information-share-point-modal").show(SharePoint);
 */
export default class InformationSharePointModal extends LightningElement {
  /**
   * SharePoint
   * @type {Object}
   */
  sharePoint;

  /**
   * @event InformationSharePointModal.handleCloseClick
   * @description This event hides the modal. It triggers when the close button is clicked.
   */
  handleCloseClick() {
    this.template.querySelector("c-modal").hide();
  }

  /**
   * @method InformationSharePointModal.show
   * @param {Object} sharePoint - SharePoint
   * @description This method will show the information of the provided SharePoint.
   */
  @api
  show(sharePoint) {
    this.sharePoint = sharePoint;
    this.template.querySelector("c-modal").show();
  }
}
