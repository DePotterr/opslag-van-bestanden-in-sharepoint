import { LightningElement, api } from "lwc";

const TEXT_MULTIPLE = "Are you sure you want to delete these files?";
const TEXT_SINGLE = "Are you sure you want to delete this file?";

/**
 * This modal is used to confirm a deletion. After confirmation, an event will dispatch. You can assign a function to that event.
 * It is a child component of Modal.
 * @alias DeleteSharePointFileModal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-delete-share-point-file-modal ondelete={function}></c-delete-share-point-file-modal>
 * Show the modal:
 * this.template.querySelector("c-delete-share-point-file-modal").show(files);
 */
export default class DeleteSharePointFileModal extends LightningElement {
  files = [];
  text = "";
  handleCancelClick() {
    this.template.querySelector("c-modal").hide();
  }

  handleDeleteClick() {
    this.dispatchEvent(
      new CustomEvent("delete", { detail: { files: this.files } })
    );
    this.template.querySelector("c-modal").hide();
  }

  @api
  show(files) {
    console.log(files);
    this.files = files;
    this.setText();
    this.template.querySelector("c-modal").show();
  }

  get getText() {
    return this.text;
  }

  setText() {
    if (this.files.length > 1) {
      this.text = TEXT_MULTIPLE;
    } else {
      this.text = TEXT_SINGLE;
    }
  }
}
