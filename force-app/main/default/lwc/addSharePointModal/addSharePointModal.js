import { api, LightningElement, track, wire } from "lwc";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import SHAREPOINT_CUSTOM_SETTING from "@salesforce/schema/SharePoint__c";

/**
 * This modal is used to add a SharePoint. It contains a form that gets validated.
 * After submit an event will be dispatched. An other component can hook a function to that event.
 * @alias AddSharePointModal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-add-share-point-modal onadd={function}></c-add-share-point-modal>
 */
export default class AddSharePointModal extends LightningElement {
  @track fieldsInfo;
  name;
  hostname;
  sitename;
  baseUrl;

  /**
   * @event AddSharePointModal.wiredSharePointInfo
   * @param {string} objectApiName - The object api name.
   * @description Wired functions that loads object information from Salesforce. In this case it is used to show the help messages.
   */
  @wire(getObjectInfo, { objectApiName: SHAREPOINT_CUSTOM_SETTING })
  wiredSharePointInfo(result) {
    if (result.data) this.fieldsInfo = result.data.fields;
    console.log(JSON.stringify(result));
  }

  /**
   * @method AddSharePointModal.isInputValid
   * @description This method is used to check if all the input fields
   * that we need to validate are valid or not.
   * @return {boolean} Is form valid?
   */
  isInputValid() {
    let isValid = true;
    let inputFields = this.template.querySelectorAll(".validate");
    inputFields.forEach((inputField) => {
      if (!inputField.checkValidity()) {
        inputField.reportValidity();
        isValid = false;
      }
    });
    return isValid;
  }

  handleCancelClick() {
    this.template.querySelector("c-modal").hide();
  }

  handleAddClick() {
    if (this.isInputValid()) {
      this.dispatchEvent(
        new CustomEvent("add", {
          detail: {
            name: this.name,
            hostname: this.hostname,
            sitename: this.sitename,
            baseUrl: this.baseUrl
          }
        })
      );
      this.template.querySelector("c-modal").hide();
    }
  }

  @api
  show() {
    this.template.querySelector("c-modal").show();
  }

  handleName(event) {
    this.name = event.target.value;
  }

  handleBaseUrl(event) {
    this.baseUrl = event.target.value;
  }

  handleHostname(event) {
    this.hostname = event.target.value;
  }

  handleSitename(event) {
    this.sitename = event.target.value;
  }
}
