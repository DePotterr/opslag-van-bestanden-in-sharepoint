import { api, LightningElement } from "lwc";

/**
 * This modal will let you make a new folder in SharePoint. It contains a form that gets validated.
 * After submit an event will be dispatched. An other component can hook a function to that event.
 * @alias CreateFolderModal
 * @extends LightningElement
 * @hideconstructor
 * @author Robin de Potter
 *
 * @example
 * <c-create-folder-modal onsubmit={function}></c-create-folder-modal>
 */
export default class CreateFolderModal extends LightningElement {
  folderName;

  @api
  show() {
    this.template.querySelector("c-modal").show();
  }

  /**
   * @method CreateFolderModal.isInputValid
   * @description This method is used to check if all the input fields
   * that we need to validate are valid or not.
   * @return {boolean} Is form valid?
   */
  isInputValid() {
    let isValid = true;
    let inputFields = this.template.querySelectorAll(".validate");
    inputFields.forEach((inputField) => {
      if (!inputField.checkValidity()) {
        inputField.reportValidity();
        isValid = false;
      }
    });
    return isValid;
  }

  handleCancelClick() {
    this.template.querySelector("c-modal").hide();
  }

  handleCreateFolderClick() {
    if (this.isInputValid()) {
      this.dispatchEvent(
        new CustomEvent("submit", { detail: { folderName: this.folderName } })
      );
      this.template.querySelector("c-modal").hide();
    }
  }

  handleFolderNameChange(event) {
    this.folderName = event.target.value;
  }
}
