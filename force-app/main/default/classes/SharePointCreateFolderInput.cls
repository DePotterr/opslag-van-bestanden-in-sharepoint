/**
 * @author Robin de Potter
 * @date 2022
 *
 * @description Object that is used for flows. The trigger will accept this class so you can add mulitple parameters.
 */
public class SharePointCreateFolderInput {
  @InvocableVariable
  public String accountId;

  @InvocableVariable
  public String sharePointSiteId;
}
