@isTest
public class SharePointServiceTest {
  public class MockGetDriveItemsHttpResponse implements HttpCalloutMock {
    public Httpresponse respond(HttpRequest req) {
      httpResponse res = new HttpResponse();
      res.setStatus('OK');
      res.setStatusCode(200);
      res.setbody('Items');
      return res;
    }
  }

  @IsTest
  static void testCreateBaseUrl() {
    String path = 'path';
    String accountId = 'accountId';
    String siteId = 'siteId';
    String sharePointName = 'test';
    String baseUrl = 'url';
    String link = 'link';

    SharePoint__c sharePoint = new SharePoint__c();
    sharePoint.Name = sharePointName;
    sharePoint.Active__c = true;
    sharePoint.Base_Url__c = baseUrl;
    sharePoint.SharePoint_Site_Id__c = siteId;
    sharePoint.Link__c = link;
    insert sharePoint;

    String url = SharePointService.createBaseUrl(accountId, path);
    Test.startTest();
    System.assertEquals(
      'callout:Graph_API/' +
      siteId +
      '/drive/root:/' +
      baseUrl +
      '/' +
      accountId +
      '/' +
      path,
      url
    );
    Test.stopTest();
  }

  @IsTest
  static void testGetDriveItems() {
    String path = 'path';
    String accountId = 'accountId';
    String siteId = 'siteId';
    String sharePointName = 'test';
    String baseUrl = 'url';
    String link = 'link';

    SharePoint__c sharePoint = new SharePoint__c();
    sharePoint.Name = sharePointName;
    sharePoint.Active__c = true;
    sharePoint.Base_Url__c = baseUrl;
    sharePoint.SharePoint_Site_Id__c = siteId;
    sharePoint.Link__c = link;
    insert sharePoint;

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new MockGetDriveItemsHttpResponse());
    String res = SharePointService.getDriveItems('accountId', 'path');
    System.assertEquals('Items', res);
    Test.stopTest();
  }
}
