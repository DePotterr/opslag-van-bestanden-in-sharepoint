/**
 * @author Robin de Potter
 * @date 2022
 *
 * @description This class will encode a string. The encoded string is typically used for http requests.
 */
public with sharing class EncodingUrl {
  public EncodingUrl() {
  }

  /**
   * @description Returns the encoded string.
   * @param stringToEncode the string that will be encoded.
   * @return the encoded string.
   */
  public static String encode(String stringToEncode) {
    return EncodingUtil.urlEncode(stringToEncode, 'UTF-8')
      .replaceAll('\\+', '%20');
  }

  /**
   * @description Returns the encoded string.
   * @param url the url that needs to be encoded.
   * @return the encoded string.
   */
  public static String encodeUrl(String url) {
    return EncodingUrl.encode(EncodingUtil.urlDecode(url, 'utf-8'));
  }
}
