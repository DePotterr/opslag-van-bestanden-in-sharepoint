/**
 * @author Robin de Potter
 * @date 2022
 *
 * @description Custom exception implementation.
 */
public virtual class SharePointException extends Exception {
}
