/**
 * @author Robin de Potter
 * @date 2022
 *
 * @description Test class for EncodingUrl
 */
@isTest
public class EncodingUrlTest {
  @isTest
  static void testEncodeUrl() {
    String url = '12. TCL Academy/Stages/Files';
    System.assertEquals(
      EncodingUtil.urlEncode(url, 'UTF-8').replaceAll('\\+', '%20'),
      EncodingUrl.encodeUrl(url)
    );
  }

  @isTest
  static void testAlreadyEncodedUrl() {
    String url = '12.%20TCL%20Academy';
    System.assertEquals(url, EncodingUrl.encodeUrl(url));
  }

  @isTest
  static void testInvalidEncodedUrl() {
    String url = '12. TCL%20Academy';
    System.assertEquals('12.%20TCL%20Academy', EncodingUrl.encodeUrl(url));
  }

  @isTest
  static void testInvalidEncodedUrl2() {
    String url = '12.+TCL%20Academy';
    System.assertEquals('12.%20TCL%20Academy', EncodingUrl.encodeUrl(url));
  }
}
