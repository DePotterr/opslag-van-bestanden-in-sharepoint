public with sharing class SharePointSettingsService {
  public SharePointSettingsService() {
  }

  @InvocableMethod(
    label='Get Current SharePoint'
    description='Returns current SharePoint.'
  )
  public static List<SharePoint__c> getCurrentSharePointAction() {
    try {
      SharePoint__c sharePoint = [
        SELECT id, Name, Active__c, Base_Url__c, SharePoint_Site_Id__c, Link__c
        FROM SharePoint__c
        WHERE Active__c = TRUE
      ];
      List<SharePoint__c> sharePointList = new List<SharePoint__c>();
      sharePointList.add(sharePoint);
      return sharePointList;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static List<SharePoint__c> getSharePointSettings() {
    try {
      return SharePoint__c.getall().values();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static List<SharePoint__c> changeSharePoint(string id) {
    try {
      Boolean hasAdminPermission = FeatureManagement.checkPermission(
        'SharePoint_Admin'
      );
      if (hasAdminPermission) {
        List<SharePoint__c> spToUpdate = new List<SharePoint__c>();
        SharePoint__c selectedSharePoint = [
          SELECT id, Active__c
          FROM SharePoint__c
          WHERE id = :id
        ];
        selectedSharePoint.Active__c = true;
        spToUpdate.add(selectedSharePoint);

        SharePoint__c currentSharePoint = getCurrentSharePoint();
        if (currentSharePoint != null) {
          currentSharePoint.Active__c = false;
          spToUpdate.add(currentSharePoint);
        }
        update spToUpdate;
        return spToUpdate;
      } else {
        throw new SharePointException('Access Denied');
      }
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static SharePoint__c getCurrentSharePoint() {
    try {
      SharePoint__c[] currentSharePoints = [
        SELECT id, Name, Active__c, Base_Url__c, SharePoint_Site_Id__c, Link__c
        FROM SharePoint__c
        WHERE Active__c = TRUE
      ];
      if (currentSharePoints.size() > 0)
        return currentSharePoints[0];
      else
        return null;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled(cacheable=false)
  public static SharePoint__c createSharePoint(
    String name,
    String hostname,
    String sitename,
    String baseUrl
  ) {
    try {
      SharePoint__c sharePoint = new SharePoint__c();
      Map<String, Object> data = (Map<String, Object>) JSON.deserializeUntyped(
        getSharePointId(hostname, sitename)
      );

      String displayName = (String) data.get('displayName');
      String siteId = (String) data.get('id');
      siteId = siteId.split(',')[1];
      String webUrl = (String) data.get('webUrl');
      String link = webUrl + '/Shared Documents';
      if (baseUrl != null)
        link = link + '/' + baseUrl;

      sharePoint.Name = name;
      sharePoint.Host_Name__c = hostname;
      sharePoint.Site_Name__c = sitename;
      sharePoint.Active__c = false;
      sharePoint.Base_Url__c = baseUrl;
      sharePoint.SharePoint_Site_Id__c = siteId;
      sharePoint.Link__c = link;
      insert sharePoint;
      return sharePoint;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static void deleteSharePoint(String id) {
    try {
      System.debug(id);
      SharePoint__c sharePoint = [
        SELECT id, Active__c
        FROM SharePoint__c
        WHERE id = :id
      ];
      if (sharePoint.Active__c)
        throw new SharePointException('Cannot delete an active sharepoint');
      delete sharePoint;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static string updateSharePoints(Object data) {
    try {
      System.debug(data);
      List<SharePoint__c> sharePointsForUpdate = (List<SharePoint__c>) JSON.deserialize(
        JSON.serialize(data),
        List<SharePoint__c>.class
      );
      update sharePointsForUpdate;
      return 'Success: SharePoints updated successfully';
    } catch (Exception e) {
      return 'The following exception has occurred: ' + e.getMessage();
    }
  }

  @AuraEnabled
  public static string getSharePointId(String hostname, String sitename) {
    try {
      HttpRequest req = new HttpRequest();
      req.setEndpoint(
        'callout:Graph_API/' +
        hostname +
        '.sharepoint.com:/sites/' +
        sitename
      );
      req.setHeader('Accept', '*/*');
      req.setMethod('GET');
      Http http = new Http();
      HTTPResponse res = http.send(req);
      return res.getBody();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }
}
