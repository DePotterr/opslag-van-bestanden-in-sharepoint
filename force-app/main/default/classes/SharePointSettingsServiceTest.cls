@isTest
public class SharePointSettingsServiceTest {
  public class MockGetDriveItemsHttpResponse implements HttpCalloutMock {
    public Httpresponse respond(HttpRequest req) {
      httpResponse res = new HttpResponse();
      res.setStatus('OK');
      res.setStatusCode(200);
      res.setbody('Items');
      return res;
    }
  }

  @IsTest
  static void testGetCurrentSharePoint() {
    SharePoint__c sharePoint = new SharePoint__c();
    sharePoint.Name = 'test';
    sharePoint.Active__c = true;
    sharePoint.Base_Url__c = 'baseUrl';
    sharePoint.SharePoint_Site_Id__c = 'site Id';
    sharePoint.Link__c = 'link';
    insert sharePoint;

    SharePoint__c currentSharePoint = SharePointSettingsService.getCurrentSharePoint();
    Test.startTest();
    System.assertEquals(sharePoint, currentSharePoint);
    Test.stopTest();
  }
}
