/**
 * @author Robin de Potter
 * @date 2022
 *
 * @description This service is the connection between Salesforce and SharePoint.
 * Security is handled whitin Salesforce. That's why this class is using callouts. Salesforce can invoke the security and make the endcall to the Microsoft Graph API.
 * The frontend (LWC) can only use @AuraEnabled methods.
 */
public with sharing class SharePointService {
  static final String CALLOUT = 'callout:Graph_API/';
  static final String DRIVE = 'drive/root:/';
  static final String UPLOAD_SESSION = ':/createUploadSession';
  static final String CONTENT = ':/content';
  static final String CHILDREN = ':/children';
  public String accessToken = '';

  public SharePointService() {
  }

  // private String authorizeUser(String username, String password) {
  //   // Azure_Active_Directory_Auth__mdt azureAuth = getAzureAppDetails('Azure_APP');
  //   // username='depotro@cronos.be';
  //   // password='123';
  //   // String clientId = 'client_id=';
  //   // Http h = new Http();
  //   // HttpRequest req = new HttpRequest();
  //   // req.setMethod('GET');
  //   // String endpoint = 'https://login.microsoftonline.com/';
  //   // req.setEndpoint(endpoint);
  //   return '';
  // }

  // private String getAccessToken() {
  //   // Authorize user
  //   // GET REQUEST
  //   if (String.isEmpty(accessToken)) {
  //     Azure_Active_Directory_Auth__mdt azureAuth = getAzureAppDetails(
  //       'Azure_APP'
  //     );
  //     String utf8 = 'UTF-8';
  //     String endpoint =
  //       'https://login.microsoftonline.com/' +
  //       EncodingUtil.urlEncode(azureAuth.Tenant_Id__c, utf8)
  //         .replaceAll('\\+', '%20') +
  //       '/oauth2/v2.0/token';
  //     Http h = new Http();
  //     HttpRequest req = new HttpRequest();
  //     req.setMethod('POST');
  //     req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
  //     req.setEndpoint(endpoint);
  //     String body =
  //       'grant_type=' +
  //       azureAuth.Azure_Grant_Type__c +
  //       '&client_id=' +
  //       EncodingUtil.urlEncode(azureAuth.Client_Id__c, utf8) +
  //       '&client_secret=' +
  //       EncodingUtil.urlEncode(azureAuth.Client_Secret__c, utf8) +
  //       '&scope=' +
  //       azureAuth.Azure_Scope__c +
  //       '&code=' +
  //       '0.AQIAA9fDSXk1v0eoiHyRP73O2ZAq0IOSBxFMoBJdcwqW9uECALo.AQABAAIAAAD--DLA3VO7QrddgJg7WevrmB_B0laSCb0zNxTUdU3WKyudz71QHyWAn8NMKpC1L871FUBfaDxf3TkzGetn5aHpM3BtSIl5ZP_n3qyA1rm7sufFQzUTd39hQSb6Q2ga3J1exPaXR_m2Sl6KmsOdSp2EP3fiMRMyX5lwTnWlmLw3kKca1v7o7oS2PAcGuHECMk2ckAm_0ThuadIkTxP5q19PoEX-GLpzLUHbfCYKETj7VpTYrPy4g1Qrygg_obCXZUQi3V78DTdIJj8y50cxLDXoiUrS5Imm5D5prnTmVOqLFl-w_0_9163agcGo0L_FdkPINKVft-mS6-gQEFtOXKao3GnnB-eL62mJ2vt_YEd2QIjFRfVh9b61C1q83tnBaCCV4lYkCImzsXlt9tHVZFRdAepV87vLCPaFZ21tpEWc1TKepY4tbz4HCi__PQqeYRTazdGrJ7NmlTfvcBIk-3oKVebYDV-aXAuvPPuLMUBNu1DNH1vOQFa8Yu3N-RAY9_1XLoFFljV1UDxd2V4q1Gbbk9784Qc0Aqh6S8UisV_HKgBfUXOuOtYF6xBm4KXXzHqre2HdTDTVhZI9QBOuqv0NYtKHulB4CGZcr25SVoLiHzbSGTx69l4a7lnE5ix74WPBwVDmFqJre7wFLb4BJjpQ_Ea3XCM3t-7fV9DlJD542CDmsRLsvG7T4SeOKT4B2WCD4UprkGbW7qWZ21n1Ggz4IAA';
  //     req.setBody(body);
  //     HttpResponse res = h.send(req);
  //     if (res.getStatus().equals('200')) {
  //       return res.getBody();
  //     } else {
  //       return '';
  //     }
  //   }
  //   return accessToken;
  // }

  // private Azure_Active_Directory_Auth__mdt getAzureAppDetails(
  //   String developerName
  // ) {
  //   String query =
  //     'SELECT ' +
  //     'MasterLabel, DeveloperName, Client_Id__c, Client_Secret__c, Azure_Scope__c, ' +
  //     'Tenant_Id__c, Azure_Grant_Type__c, SharePoint_Site_Id__c ' +
  //     'FROM Azure_Active_Directory_Auth__mdt WHERE DeveloperName =: developerName';
  //   List<Azure_Active_Directory_Auth__mdt> azureAuths = Database.query(
  //     String.escapeSingleQuotes(query)
  //   );
  //   return azureAuths[0];
  // }

  /**
   * @description It creates a base url that can be used to make http calls.
   * @param accountId the id of an account
   * @param path account subfolder as a string
   * @return the base url
   */
  public static String createBaseUrl(String accountId, String path) {
    SharePoint__c sharePoint = SharePointSettingsService.getCurrentSharePoint();
    String site = sharePoint.SharePoint_Site_Id__c + '/';
    String baseUrl = EncodingUrl.encodeUrl(sharePoint.Base_Url__c) + '/';
    accountId = EncodingUrl.encodeUrl(accountId);
    if (path == null || path.equals(''))
      path = '';
    else
      path = '/' + path;
    return CALLOUT + site + DRIVE + baseUrl + accountId + path;
  }

  /**
   * @description This will create an upload session where a file can be uploaded to.
   * @param accountId the id of an account
   * @param path account subfolder as a string
   * @param filename the name of the file
   * @return an upload url
   */
  @AuraEnabled
  public static String createUploadSession(
    String accountId,
    String path,
    String filename
  ) {
    try {
      HttpRequest req = new HttpRequest();
      String encodedFilename = EncodingUrl.encode(filename);
      String endpoint =
        createBaseUrl(accountId, path) +
        '/' +
        encodedFilename +
        UPLOAD_SESSION;
      System.debug(endpoint);
      String body = '{"item": {"@microsoft.graph.conflictBehavior": "rename"}}';
      req.setEndpoint(endpoint);
      req.setHeader('Accept', '*/*');
      req.setMethod('POST');
      req.setHeader('Content-Type', 'application/json');
      req.setBody(body);
      Http http = new Http();
      HTTPResponse res = http.send(req);
      return res.getBody();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This will return drive items.
   * @param accountId the id of an account
   * @param path account subfolder as a string
   * @return It returns items (files) from a specific account and folder
   */
  @AuraEnabled
  public static String getDriveItems(String accountId, String path) {
    try {
      System.debug(path);
      HttpRequest req = new HttpRequest();
      String endpoint = createBaseUrl(accountId, path) + CHILDREN;
      System.debug(endpoint);
      req.setEndpoint(endpoint);
      req.setHeader('Accept', '*/*');
      req.setMethod('GET');
      Http http = new Http();
      HTTPResponse res = http.send(req);
      return res.getBody();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in flows to create a new folder.
   * This comes in handy when you have a specific business rule such as creating a folder when a new account is created.
   * @param Ids A list that contains the id of an account and a SharePoint site
   * @return It returns null
   */
  @InvocableMethod(
    label='Create folder'
    description='Creates a folder in SharePoint with accountId as name.'
  )
  public static List<String> createFolderAction(
    List<SharePointCreateFolderInput> Ids
  ) {
    try {
      System.debug(Ids);
      createFolderCallout(Ids.get(0).accountId, Ids.get(0).sharePointSiteId);
      return null;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This is an http callout that is being used in the createFolderAction.
   * @param accountId the id of an account
   * @param sharePointSiteId the id of a SharePoint site
   */
  @future(callout=true)
  public static void createFolderCallout(
    String accountId,
    String sharePointSiteId
  ) {
    HttpRequest req = new HttpRequest();
    String body = '{"name": "' + accountId + '","folder": {}}';
    req.setEndpoint(
      'callout:Graph_API/' +
      sharePointSiteId +
      '/drive/root:/12.%20TCL%20Academy/Stages/Files:/children'
    );
    req.setHeader('Accept', '*/*');
    req.setHeader('Content-Type', 'application/json');
    req.setMethod('POST');
    req.setBody(body);
    Http http = new Http();
    http.send(req);
  }

  /**
   * @description This method can be used in the frontend to create folders.
   * @param accountId the id of an account
   * @param path account subfolder as a string
   * @param folderName the name of a folder that will be created
   * @return It returns the folder that is created
   */
  @AuraEnabled
  public static String createFolder(
    String accountId,
    String path,
    String folderName
  ) {
    try {
      HttpRequest req = new HttpRequest();
      SharePoint__c sharePoint = SharePointSettingsService.getCurrentSharePoint();
      String body = '{"name": "' + folderName + '","folder": {}}';
      String endpoint = createBaseUrl(accountId, path) + CHILDREN;
      req.setEndpoint(endpoint);
      req.setHeader('Accept', '*/*');
      req.setHeader('Content-Type', 'application/json');
      req.setMethod('POST');
      req.setBody(body);
      Http http = new Http();
      HTTPResponse res = http.send(req);
      return res.getBody();
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to delete an item.
   * @param id the id of an item (file or folder)
   * @return It returns an empty string
   */
  @AuraEnabled(cacheable=true)
  public static String deleteItem(String id) {
    try {
      Boolean hasDeletePermission = FeatureManagement.checkPermission(
        'SharePoint_Delete_File'
      );
      if (hasDeletePermission) {
        HttpRequest req = new HttpRequest();
        SharePoint__c sharePoint = SharePointSettingsService.getCurrentSharePoint();
        req.setEndpoint(
          'callout:Graph_API/' +
          sharePoint.SharePoint_Site_Id__c +
          '/drive/items/' +
          id
        );
        req.setHeader('Accept', '*/*');
        req.setMethod('DELETE');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();
      } else {
        throw new SharePointException('Access Denied');
      }
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to delete an multiple items.
   * @param id a list of item ids
   * @return It returns an empty string
   */
  @AuraEnabled(cacheable=true)
  public static List<String> deleteItems(List<Map<String, String>> files) {
    try {
      Boolean hasDeletePermission = FeatureManagement.checkPermission(
        'SharePoint_Delete_File'
      );
      if (hasDeletePermission) {
        String[] bodies = new List<String>{};
        for (Map<String, String> file : files) {
          string body = deleteItem(file.get('id'));
          bodies.add(body);
        }
        return bodies;
      } else {
        throw new SharePointException('Access Denied');
      }
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to upload an file.
   * @param accountId the id of an account
   * @param filename the name of the file that will be created
   * @param path account subfolder as a string
   * @param base64 the body of the file as a base64 string
   * @param contentType the type of the file
   * @return It returns the file that is created
   */
  @AuraEnabled(cacheable=false)
  public static String uploadItem(
    String accountId,
    String filename,
    String path,
    String base64,
    String contentType
  ) {
    try {
      Boolean hasUploadPermission = FeatureManagement.checkPermission(
        'SharePoint_Upload_File'
      );
      if (hasUploadPermission) {
        Blob itemBlob = EncodingUtil.base64Decode(base64);
        HttpRequest req = new HttpRequest();
        String encodedFilename = EncodingUrl.encode(filename);
        String endpoint =
          createBaseUrl(accountId, path) +
          '/' +
          encodedFilename +
          CONTENT +
          '?@microsoft.graph.conflictBehavior=rename';
        req.setEndpoint(endpoint);
        req.setHeader('Accept', '*/*');
        req.setHeader('Content-Type', contentType);
        req.setMethod('PUT');
        req.setBodyAsBlob(itemBlob);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();
      } else {
        throw new SharePointException('Access Denied');
      }
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to upload multiple files. It loops over the uploadItem method.
   * @param files list of file information
   * @return It returns the files that are created
   */
  @AuraEnabled(cacheable=false)
  public static String[] uploadItems(
    String accountId,
    List<Map<String, String>> files
  ) {
    try {
      Boolean hasUploadPermission = FeatureManagement.checkPermission(
        'SharePoint_Upload_File'
      );
      if (hasUploadPermission) {
        String[] bodies = new List<String>{};
        System.debug(files);
        for (Map<String, String> file : files) {
          String body = uploadItem(
            accountId,
            file.get('filename'),
            file.get('path'),
            file.get('base64'),
            file.get('contentType')
          );
          bodies.add(body);
        }
        return bodies;
      } else {
        throw new SharePointException('Access Denied');
      }
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to get the column width of the datatable.
   * @return It returns the column widths in an array (as a string).
   */
  @AuraEnabled
  public static DataTable_Column_Width_SharePoint__c getDataTableColumnWidthSharePoint() {
    try {
      return DataTable_Column_Width_SharePoint__c.getAll().values()[0];
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /**
   * @description This method can be used in the frontend to set the column width of the datatable.
   * @param newColumnWidth String the new column widths
   */
  @AuraEnabled
  public static void setDataTableColumnWidthSharePoint(String newColumnWidth) {
    try {
      System.debug(newColumnWidth);
      DataTable_Column_Width_SharePoint__c columnWidth = DataTable_Column_Width_SharePoint__c.getAll()
        .values()[0];
      columnWidth.Column_Width__c = newColumnWidth;
      update columnWidth;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }
}
